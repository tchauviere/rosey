import React, { Component } from 'react';
import { Alert, StyleSheet, Image } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Container, Header, Button, Text, Body, Form, Item as FormItem, Input, Label, Title, View} from 'native-base';
import DeviceInfo from 'react-native-device-info';
//import DeviceInfo which will help to get UniqueId


export default class Signup extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      phonemaileNumber: '',
      deviceId: '',
      isReady: false,
      isLogged: false,
    };
  }

  /*componentDidMount() {
   
  }*/
  
  onVerifyEmail() {
    const { email } = this.state;
    
    try {
      let id = DeviceInfo.getUniqueID();
      AsyncStorage.setItem('isLogged', id);
      this.setState({ deviceId: id });
      Alert.alert('ID Device', id);
    } catch (error) {
      Alert.alert('An error occured while checking your email, please try again.');
    }
  }

  async isLogged() {
    try {
        let isLogged = await AsyncStorage.getItem('isLogged');
//console.warn(isLogged);
        if (isLogged) {
          alert('logged');
          return true;
        }

        return false;
    } catch (err) {
      return false;
    }
  }

  render() {

    if (this.isLogged()) {
      this.props.navigation.replace('Home');
    } 
    
    return (
      <Container>
        <Header>
          <Body>
            <Title>Rosey !</Title>
          </Body>
        </Header>
        <Form style={{padding: 15}}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Image  source={require('./images/logo.png')} style={{height: 250, width: 250, }}/>
          </View>
          <FormItem floatingLabel>
            <Label>Email</Label>
            <Input value={this.state.email}
                  onChangeText={(email) => this.setState({ email })}
                  placeholder={'Email'}
                  keyboardType="email-address" />
          </FormItem>
          <Button full primary onPress={this.onVerifyEmail.bind(this)} style={{marginTop: 15}} >
            <Text>Sign Up</Text>
          </Button>
        </Form>
        
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    marginBottom: 10,
  },
});
