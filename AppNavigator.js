import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Signup from './Signup';
import Home from './Home';

const RootStack = createStackNavigator({
  Signup: {
    screen: Signup,
    navigationOptions: {
      title: "Signup",
      headerLeft: null
    },
  },
  Home: {
    screen: Home,
    navigationOptions: {
      title: "Home",
      headerLeft: null
    },
  }
}, 
{ 
  initialRouteName: 'Signup'
});

const App = createAppContainer(RootStack);

export default App;