import React, { Component } from 'react';
import { ActivityIndicator, StyleSheet } from 'react-native';
import { Container, Header, Button, Text, Body, Title, View } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';


export default class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isReady: false,
    };
  }

  componentDidMount() {
    this.setState({ isReady: true });
  }

  async onDisconnect() {
    try {
      await AsyncStorage.removeItem('isLogged');
      this.props.navigation.navigate('Signup');
    } catch (err) {
      alert('Unable to disconnect. Please try again.');
    }
  }

  render() {

    if (!this.state.isReady) {
      return (
        <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    }

    return (
      <Container>
        <Header>
          <Body>
            <Title>Rosey Home !</Title>
          </Body>
        </Header>
        <Button full primary style={{ marginTop: 15 }} >
          <Text>Say Rosey !</Text>
        </Button>
        <Button full primary style={{ marginTop: 15 }} onPress={this.onDisconnect.bind(this)}>
          <Text>Disconnect</Text>
        </Button>
      </Container>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
})
